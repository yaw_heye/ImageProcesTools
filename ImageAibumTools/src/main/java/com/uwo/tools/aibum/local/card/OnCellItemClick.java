package com.uwo.tools.aibum.local.card;

import android.view.View;

public interface OnCellItemClick {
    public void onCellClick(View v, CardGridItem item);
}
