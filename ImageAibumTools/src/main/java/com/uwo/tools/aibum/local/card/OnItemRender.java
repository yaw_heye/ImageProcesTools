package com.uwo.tools.aibum.local.card;


public interface OnItemRender {
	
	public void onRender(CheckableLayout v, CardGridItem item);

}
